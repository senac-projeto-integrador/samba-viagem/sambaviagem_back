package com.sambaonibus.samba_onibus.Util;

import java.util.Random;

public class GeradorDataMaior {

    public String geraData()
    {
        String date = "";
        String mesAsString;
        String diaAsString;
        //1970-06-02
            Random rand = new Random();
            //int randAno = rand.nextInt((60) + 50);

        int AnoMax = 99; //Máximo ano
        int AnoMin = 60; //Menor ano possível
        int randAno = AnoMin + (int)(Math.random() * ((AnoMax - AnoMin) + 1));

        int MesMax = 12; //Máximo mes
        int MesMin = 1; //Menor mes possível
        int randMes = MesMin + (int)(Math.random() * ((MesMax - MesMin) + 1));
        if (randMes < 10){
            mesAsString = Integer.toString(randMes);
            mesAsString = "0" + randMes;
        } else {
            mesAsString = Integer.toString(randMes);
        }

        int DiaMax = 29; //Máximo dia
        int DiaMin = 1; //Menor dia possível
        int randDia = DiaMin + (int)(Math.random() * ((DiaMax - DiaMin) + 1));

        if (randDia < 10){
            diaAsString = Integer.toString(randDia);
            diaAsString = "0" + randDia;
        } else {
            diaAsString = Integer.toString(randDia);
        }
            date = "19"+randAno+"-"+mesAsString+"-"+diaAsString;
        return date;

    }
    String data = geraData();
    @Override
    public String toString() {
        return data;
    }
}
