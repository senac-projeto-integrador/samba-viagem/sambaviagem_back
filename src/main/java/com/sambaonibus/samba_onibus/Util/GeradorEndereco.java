package com.sambaonibus.samba_onibus.Util;

import java.util.Random;

public class GeradorEndereco {
String endereco = "";
    public String geraEnd() {


        String[] logradouro = {"Av.", "Rua", "R.", "Avenida"};
        Random rand = new Random();
        int randLogra = rand.nextInt(4);
        endereco = logradouro[randLogra] + " ";

        if (randLogra < 2){
            //nome simples
            String[] nomeSimples = {"Camacho", "Campinas", "Riachuello", "da Alegria",
                    "do Sol", "da Maresia", "Salso", "Principal", "Amazonas", "Sete de Setembro",
                    "Primeiro de Maio", "20 de Julho", "Dom Pedro II", "Santa Rita"};
            randLogra = rand.nextInt(nomeSimples.length);
            endereco = endereco + nomeSimples[randLogra];
        } else {
            //nome composto
            String[] composto1 = {"Sebastião", "Florêncio", "Duque", "Barão", "Santo", "Felício", "São", "Rui", "João", "Pedro"};
            String[] composto2 = {"Gama", "Flores", "Cunha", "Caxias", "Matarazzo", "Osório", "Francisco", "Assis", "Beltrão", "Barbosa", "Dumont"};
            int randLogra1 = rand.nextInt(composto1.length);
            int randLogra2 = rand.nextInt(composto2.length);
            endereco = endereco + composto1[randLogra1] + " " + composto2[randLogra2];
        }
return endereco;
    }
    String EnderecoExport = geraEnd();

    @Override
    public String toString() {
        return endereco;
    }


}
