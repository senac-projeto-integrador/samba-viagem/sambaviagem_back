package com.sambaonibus.samba_onibus.Util;

import java.util.Arrays;
import java.util.Random;

public class Nomes {
    String[] nomes = {"José", "Antônio", "João", "Francisco", "Luiz", "Paulo", "Carlos", "Manoel", "Pedro", "Marcos", "Raimundo", "Sebastião", "Marcelo", "Jorge", "Geraldo", "Luis", "Fernando", "Fábio"
            , "Fabio", "Roberto", "Márcio", "Edson", "André", "Sérgio", "Daniel", "Rodrigo", "Rafael", "Joaquim", "Ricardo", "Eduardo", "Alexandre"
            , "Cláudio", "Leandro", "Mário", "Maicom", "Maicon", "Matheus", "Mateus", "Thiago", "Tiago","Charles", "Tiarles"
            , "Ana", "Anelise", "Betania", "Beatriz", "Bruna", "Carolina", "Carol", "Daiane", "Duane", "Eduarda", "Elaine", "Elza"
            , "Fabiane", "Fabiola", "Gabriela", "Helena", "Heloisa", "Ilza", "Laura", "Luane", "Luana", "Maiara", "Noeli", "Olga", "Paola", "Quaraira"
            , "Vilma", "Jéssica", "Sabrina", "Suelen", "Thauane", "Ugiusleia", "Vanessa", "Rosa"};


    Random rand = new Random();
    int n1 = rand.nextInt(nomes.length);
    String nomeRetorno = nomes[n1];

    @Override
    public String toString() {
        return nomeRetorno;
    }

    public String getNomes() {
        String nomeRetorno = nomes[n1];
        return nomeRetorno;
    }
}
