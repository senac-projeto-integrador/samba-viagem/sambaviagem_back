package com.sambaonibus.samba_onibus.Util;

import java.util.Random;

public class GeradorCPF {
    String rg = "";
    public String geraCPF()
    {
        rg = "";
        while (rg.length() < 11){
            Random rand = new Random();
            int rand9 = rand.nextInt(9);

            String intString = Integer.toString(rand9);
            rg = rg + intString;
        }
        return rg;
    }

    String documento = geraCPF();
    @Override
    public String toString() {
        String cpf = documento;
        return cpf;
    }
}
