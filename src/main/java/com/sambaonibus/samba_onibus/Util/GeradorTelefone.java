package com.sambaonibus.samba_onibus.Util;

import java.util.Random;

public class GeradorTelefone {


    String telefone = "";

    public String geraTelefone() {


        String[] ddd = {"51", "53", "54", "55"};
        String[] prefixo = {"91", "92", "93", "94", "84", "85", "89", "95", "98", "99"};

        Random rand = new Random();
        int randDDD = rand.nextInt(4);
        int randPrefixo = rand.nextInt(10);

        telefone = ddd[randDDD];
        telefone = telefone + "9";
        telefone = telefone + prefixo[randPrefixo];

        String corpoNumero = "";

        for (int i = 0; i < 6; i++) {
            int rand9 = rand.nextInt(9);

            String intString = Integer.toString(rand9);
            corpoNumero = corpoNumero + intString;
        }

        telefone = telefone + corpoNumero;
        return telefone;
    }


    String telefoneExport = geraTelefone();

    @Override
    public String toString() {
        return telefoneExport;
    }

}
