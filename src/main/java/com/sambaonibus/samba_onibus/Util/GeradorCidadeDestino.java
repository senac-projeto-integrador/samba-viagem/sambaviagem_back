package com.sambaonibus.samba_onibus.Util;

import java.util.Random;

public class GeradorCidadeDestino {
    String city = "";

    public String geraCidadeDestino() {

        Random rand = new Random();
        int indicador = rand.nextInt(100);
        String cidadesTop[] = {"Curitiba", "Florianópolis", "Gramado", "Penha", "Foz do Iguaçu", "Porto Alegre", "São Paulo", "Caxias do Sul"};
        String cidadesMed[] = {"Rio de Janeiro", "Balneário Camboriú", "Brasília", "Montevideo", "Buenos Aires", "Santiago", "Bonito", "Gramado"};
        String cidadesLow[] = {"Santa Maria", "Vitória", "Jaguarão", "Chuí", "Lima", "Bariloche", "Tramandaí"};
        String cidadesLowest[] = {"Canguçu", "Camaquã", "Rio Grande", "Santa da Boa Vista", "São Sepé",
                "Caçapava do Sul", "Bagé", "Feliz", "Encantado", "Três de Maio", "Boa Vista", "São Gabriel",
                "Teutônia", "Canoas", "Novo Hamburgo", "Lajeado", "São Borja", "Gravataí", "Viamão", "Santa Rosa", "Venâncio Aires", "Lajeado"};


        if (indicador > 50) {
            int seletorCidade = rand.nextInt(cidadesTop.length);
            city = cidadesTop[seletorCidade];
        } else if (indicador <= 50 && indicador >= 25) {
            int seletorCidade = rand.nextInt(cidadesMed.length);
            city = cidadesMed[seletorCidade];
        } else if (indicador < 25 && indicador >= 10) {
            int seletorCidade = rand.nextInt(cidadesLow.length);
            city = cidadesLow[seletorCidade];
        } else if (indicador < 10) {
            int seletorCidade = rand.nextInt(cidadesLowest.length);
            city = cidadesLowest[seletorCidade];
        }

        return city;
    }
    String cidade = geraCidadeDestino();
    @Override
    public String toString() {
        return city;
    }
}
