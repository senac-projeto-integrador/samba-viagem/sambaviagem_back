package com.sambaonibus.samba_onibus.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "passageiro")
//@SQLDelete(sql = "UPDATE passageiro SET deleted = 'true' WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted = 'true'")
//@NamedQuery(name = "Passageiro.BuscarTodos")

public @Data
class Passageiro {

    //TODO: Criar relacionamento das tabelas

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @ManyToMany(mappedBy = "passageiros")
    @JsonIgnore
    private List<Viagem> viagens;

    //@NotBlank(message = "Data de saída inválida")
    @Column(nullable = false, length = 255)
    private String nome;

    // TODO: Criar validação para o CPF
    //@NotBlank(message = "Nenhum CPF válido")
    @Column(nullable = false, length = 11)
    private String cpf;

    //@NotBlank(message = "Nenhum RG informado")
    @Column(unique = true, length = 10)
    private String rg;

    //@NotBlank(message = "Necessita de uma data de nascimento")
    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate nascimento;

    @Column()
    private String telefone;

    @Column()
    private String endereco;

    @Column()
    private String cidade;

    @Column()
    private Timestamp created_at;

    @Column()
    private Timestamp updated_at;

    @Column(nullable = false)
    private boolean deleted;

    public List<Viagem> getViagens() {
        return viagens;
    }

    public void setViagens(List<Viagem> viagens) {
        this.viagens = viagens;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
