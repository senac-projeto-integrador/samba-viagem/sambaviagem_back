package com.sambaonibus.samba_onibus;

import com.sambaonibus.samba_onibus.Util.*;
import com.sambaonibus.samba_onibus.controller.PassageiroController;
import com.sambaonibus.samba_onibus.domain.Motorista;
import com.sambaonibus.samba_onibus.domain.Passageiro;
import com.sambaonibus.samba_onibus.repository.MotoristaRepository;
import com.sambaonibus.samba_onibus.repository.PassageiroRepository;
import com.sun.xml.internal.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static java.lang.String.valueOf;
import static javafx.scene.input.KeyCode.T;


@Configuration
@EnableScheduling
@SpringBootApplication
public class SambaOnibusApplication {

    public static void main(String[] args) {
        SpringApplication.run(SambaOnibusApplication.class, args);
    }


    //Vai executar a cada 10 segundos



        //ResponseEntity

    }


